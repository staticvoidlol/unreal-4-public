UPhysicsHandleComponent cannot be extended because "its UCLASS macro has the MinimalAPI specifier" according to Doug E. 
Please see https://answers.unrealengine.com/questions/715008/ue-4172-unresolved-external-when-extending-uphysic.html for more details.

This is a workaround hack which re-implements the PhysicsHandler as a custom ActorComponent, so be aware that Epic's changes to UPhysicsHandleComponent 
will NOT be reflected here. You will have to manually update this class.

How to use:

1) Ensure that you add PhysX to your YOURGame.Build.cs (you might need to clean out your intermediate folder and regenerate your VS files - not sure if it's required but I've had some issues with this in the past):

PrivateDependencyModuleNames.AddRange(new string[] { "PhysX", "APEX" });

2) If you are ok with having this class be called "PWNPhysicsHandleComponent" you can simply paste these two files into your project. They must be in the same folder though. You will have to regenerate VS files to get these included. 

Otherwise, create a new class via the editor with your required name e.g. "MyPhysicsHandleComponent" and copy paste the contents of these two files into your files. 
Then find replace "PWNPhysicsHandleComponent" with "UMyPhysicsHandleComponent". It should now work.
