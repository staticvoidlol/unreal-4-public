// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Helpers/PWNMovementDirectionCalculator.h"
#include "PWNPlayerCharacter.generated.h"

UCLASS(Config=GameUserSettings)
class PWNGAME_API APWNPlayerCharacter : public ACharacter, public IPWNSerialisable
{
	GENERATED_BODY()

public:
	/** Called when the game starts or when spawned. */
	virtual void BeginPlay() override;

	/** Called to bind functionality to input. */
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/*********/
	/* Input */
	/*********/
	void InputMoveForward(float val);
	void InputMoveBackward(float val);
	void InputMoveLeft(float val);
	void InputMoveRight(float val);
	
protected:
	/******************************/
	/*** Movement Direction ***/
	/******************************/
	// MovementDirectionCalculator
	UPROPERTY()
	UPWNMovementDirectionCalculator* MovementDirectionCalculator;
	
	// How to use
	void TestMovementDirection();
	/**************************/
	
	
};
