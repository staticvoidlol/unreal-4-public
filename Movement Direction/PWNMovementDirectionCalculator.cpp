// Fill out your copyright notice in the Description page of Project Settings.

#include "PWNGame.h"
#include "PWNMovementDirectionCalculator.h"

UPWNMovementDirectionCalculator::UPWNMovementDirectionCalculator()
{
	// Set defaults
	IsMovingForward = false;
	IsMovingBackward = false;
	IsMovingRight = false;
	IsMovingLeft = false;

	// Init movement dir
	MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_NoDirection;
}

void UPWNMovementDirectionCalculator::ForwardStart(){ IsMovingForward = true; }
void UPWNMovementDirectionCalculator::ForwardStop(){ IsMovingForward = false; }
void UPWNMovementDirectionCalculator::RightStart(){ IsMovingRight = true; }
void UPWNMovementDirectionCalculator::RightStop(){ IsMovingRight = false; }
void UPWNMovementDirectionCalculator::BackwardStart(){ IsMovingBackward = true; }
void UPWNMovementDirectionCalculator::BackwardStop(){ IsMovingBackward = false; }
void UPWNMovementDirectionCalculator::LeftStart(){ IsMovingLeft = true; }
void UPWNMovementDirectionCalculator::LeftStop(){ IsMovingLeft = false; }

TEnumAsByte<EMovementDirection> UPWNMovementDirectionCalculator::GetMovementDirectionCurrent()
{
	UpdateMovementDirection();
	return MovementDirectionCurrent;
}

FVector UPWNMovementDirectionCalculator::GetMovementDirectionVector()
{
	UpdateMovementDirection();

	switch (MovementDirectionCurrent)
	{
		case MOVEMENTDIRECTION_NoDirection:
			MovementDirectionCurrentVector = FVector(0.0f, 0.0f, 0.0f);
		break;
		case MOVEMENTDIRECTION_ForwardCenter:
			MovementDirectionCurrentVector = FVector(0.0f, 1.0f, 0.0f);
		break;
		case MOVEMENTDIRECTION_ForwardRight:
			MovementDirectionCurrentVector = FVector(1.0f, 1.0f,0.0f);
		break;
		case MOVEMENTDIRECTION_CenterRight:
			MovementDirectionCurrentVector = FVector(1.0f, 0.0f, 0.0f);
		break;
		case MOVEMENTDIRECTION_BackRight:
			MovementDirectionCurrentVector = FVector(1.0f, -1.0f, 0.0f);
		break;
		case MOVEMENTDIRECTION_BackCenter:
			MovementDirectionCurrentVector = FVector(0.0f, -1.0f, 0.0f);
		break;
		case MOVEMENTDIRECTION_BackLeft:
			MovementDirectionCurrentVector = FVector(-1.0f, -1.0f, 0.0f);
		break;
		case MOVEMENTDIRECTION_CenterLeft:
			MovementDirectionCurrentVector = FVector(-1.0f, 0.0f, 0.0f);
		break;
		case MOVEMENTDIRECTION_ForwardLeft:
			MovementDirectionCurrentVector = FVector(-1.0f, 1.0f, 0.0f);
		break;
		default:
			break;
	}

	MovementDirectionCurrentVector.Normalize();

	return MovementDirectionCurrentVector;
}

void UPWNMovementDirectionCalculator::UpdateMovementDirection()
{
	int8 fwdDir = 0;
	int8 rightDir = 0;

	// Calculate forward direction
	if ((IsMovingForward && IsMovingBackward) || (!IsMovingForward && !IsMovingBackward))
		fwdDir = 0;
	else if (IsMovingForward && !IsMovingBackward)
		fwdDir = 1;
	else if (!IsMovingForward && IsMovingBackward)
		fwdDir = -1;

	// Calculate right direction
	if ((IsMovingRight && IsMovingLeft) || (!IsMovingRight && !IsMovingLeft))
		rightDir = 0;
	else if (IsMovingRight && !IsMovingLeft)
		rightDir = 1;
	else if (!IsMovingRight && IsMovingLeft)
		rightDir = -1;

	// Calculate movement direction
	if ((fwdDir == 0) && (rightDir == 0))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_NoDirection;
	}
	else if ((fwdDir == 1) && (rightDir == 0))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_ForwardCenter;
	}
	else if ((fwdDir == 1) && (rightDir == 1))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_ForwardRight;
	}
	else if ((fwdDir == 0) && (rightDir == 1))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_CenterRight;
	}
	else if ((fwdDir == -1) && (rightDir == 1))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_BackRight;
	}
	else if ((fwdDir == -1) && (rightDir == 0))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_BackCenter;
	}
	else if ((fwdDir == -1) && (rightDir == -1))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_BackLeft;
	}
	else if ((fwdDir == 0) && (rightDir == -1))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_CenterLeft;
	}
	else if ((fwdDir == 1) && (rightDir == -1))
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_ForwardLeft;
	}
	else
	{
		MovementDirectionCurrent = EMovementDirection::MOVEMENTDIRECTION_NoDirection;
	}
}
