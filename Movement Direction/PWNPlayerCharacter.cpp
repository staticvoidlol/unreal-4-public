// Called when the game starts or when spawned
void APWNPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Movement direction calculator
	MovementDirectionCalculator = NewObject<UPWNMovementDirectionCalculator>(this, UPWNMovementDirectionCalculator::StaticClass());
}

/*************/
/*** Input ***/
/*************/
// Called to bind functionality to input
void APWNPlayerCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	// Test
	InputComponent->BindAction(TEXT("InputTest"), EInputEvent::IE_Pressed, this, &APWNPlayerCharacter::InputTest);

	// Movement and Look
	InputComponent->BindAxis(TEXT("InputMoveForward"), this, &APWNPlayerCharacter::InputMoveForward);
	InputComponent->BindAxis(TEXT("InputMoveBackward"), this, &APWNPlayerCharacter::InputMoveBackward);
	InputComponent->BindAxis(TEXT("InputMoveLeft"), this, &APWNPlayerCharacter::InputMoveLeft);
	InputComponent->BindAxis(TEXT("InputMoveRight"), this, &APWNPlayerCharacter::InputMoveRight);

void APWNPlayerCharacter::InputMoveForward(float val)
{	
	if (val > 0.0f)
	{
		MovementDirectionCalculator->ForwardStart();

		if (!MovementInputIgnored)
			AddMovementInput(GetMovementViewDirection(EAxis::X), val);
	}
	else
		MovementDirectionCalculator->ForwardStop();
}

void APWNPlayerCharacter::InputMoveBackward(float val)
{
	if (val > 0.0f)
	{
		MovementDirectionCalculator->BackwardStart();

		if (!MovementInputIgnored)
			AddMovementInput(GetMovementViewDirection(EAxis::X), -1.0f * val);
	}
	else
		MovementDirectionCalculator->BackwardStop();
}

void APWNPlayerCharacter::InputMoveLeft(float val)
{
	if (val > 0.0f)
	{
		MovementDirectionCalculator->LeftStart();

		if (!MovementInputIgnored)
			AddMovementInput(GetMovementViewDirection(EAxis::Y), -1.0f * val);
	}
	else
		MovementDirectionCalculator->LeftStop();
}

void APWNPlayerCharacter::InputMoveRight(float val)
{
	if (val > 0.0f)
	{
		MovementDirectionCalculator->RightStart();

		if (!MovementInputIgnored)
			AddMovementInput(GetMovementViewDirection(EAxis::Y), val);
	}
	else
		MovementDirectionCalculator->RightStop();
}

void APWNPlayerCharacter::TestMovementDirection()
{
	EMovementDirection dir = MovementDirectionCalculator->GetMovementDirectionCurrent();
	
	switch(dir)
	{
		case EMovementDirection::MOVEMENTDIRECTION_NoDirection:
			// no direction
		break;
		case EMovementDirection::MOVEMENTDIRECTION_ForwardCenter:
			// Forward Only
		break;
		....
		....
		....
		default:
			// other
		break;
		
	}
}
	

