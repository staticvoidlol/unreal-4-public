/*******************************/
/*** Movement Direction Enum ***/
/*******************************/
UENUM(BlueprintType)
enum EMovementDirection
{
	MOVEMENTDIRECTION_NoDirection						UMETA(DisplayName = "No Direction"),
	MOVEMENTDIRECTION_ForwardCenter						UMETA(DisplayName = "Forward Center"),
	MOVEMENTDIRECTION_ForwardRight						UMETA(DisplayName = "Forward Right"),
	MOVEMENTDIRECTION_CenterRight						UMETA(DisplayName = "Center Right"),
	MOVEMENTDIRECTION_BackRight							UMETA(DisplayName = "Back Right	"),
	MOVEMENTDIRECTION_BackCenter						UMETA(DisplayName = "Back Center"),
	MOVEMENTDIRECTION_BackLeft							UMETA(DisplayName = "Back Left	"),
	MOVEMENTDIRECTION_CenterLeft						UMETA(DisplayName = "Center Left"),
	MOVEMENTDIRECTION_ForwardLeft						UMETA(DisplayName = "Forward Left")
};