// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "PWNMovementDirectionCalculator.generated.h"

/**
 * 
 */
UCLASS()
class PWNGAME_API UPWNMovementDirectionCalculator : public UObject
{
	GENERATED_BODY()

public:
	UPWNMovementDirectionCalculator();

	// Notify functions
	void ForwardStart();
	void ForwardStop();
	void RightStart();
	void RightStop();
	void BackwardStart();
	void BackwardStop();
	void LeftStart();
	void LeftStop();
	// Public function to get the current movement dir
	TEnumAsByte<EMovementDirection> GetMovementDirectionCurrent();	
	FVector GetMovementDirectionVector();

protected:	
	void UpdateMovementDirection();
	UPROPERTY()
	bool IsMovingForward;
	UPROPERTY()
	bool IsMovingBackward;
	UPROPERTY()
	bool IsMovingRight;
	UPROPERTY()
	bool IsMovingLeft;
	UPROPERTY()
	TEnumAsByte<EMovementDirection> MovementDirectionCurrent;	
	UPROPERTY()
	FVector MovementDirectionCurrentVector;
};